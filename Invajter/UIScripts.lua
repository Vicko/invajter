local LastButton = 0 -- inv fraze
function Invajter.button_minimap_Reposition()
	Invajter_button_minimap:SetPoint("TOPLEFT",52-(80*cos(InvajterSV["minimap"])),(80*sin(InvajterSV["minimap"]))-52)
end


function Invajter_button_minimap_drag_OnUpdate()

	local xpos,ypos = GetCursorPosition()
	local xmin,ymin = Minimap:GetLeft(), Minimap:GetBottom()

	xpos = xmin-xpos/UIParent:GetScale()+70
	ypos = ypos/UIParent:GetScale()-ymin-70

	InvajterSV["minimap"] = math.deg(math.atan2(ypos,xpos))
	Invajter.button_minimap_Reposition()
end


function Invajter.button_minimap_OnClick()
	if Frame then
	    Frame = nil
	    Invajter_frame:Hide()
	else
	    Frame = 1
	    Invajter_frame:Show()
	end
	if LastButton == 0 then
	    Invajter.button_fraze_OnClick()
	elseif LastButton == 1 then
	    Invajter.button_blacklist_OnClick()
	end
	Invajter_add_edit_box:SetText("")
end


function Invajter.button_blacklist_OnClick()
    LastButton = 1
    local i = 0
    Invajter_button_delete:Show()
    Invajter.HighlightLastButton()
    Invajter_scroll_edit:SetText("")
    while InvajterSV["blacklist"][i]~=nil do
        Invajter_scroll_edit:Insert(InvajterSV["blacklist"][i].."\n")
        i=i+1
    end
end


function Invajter.button_fraze_OnClick()
    LastButton = 0
    local i = 0
    Invajter_button_delete:Show()
    Invajter.HighlightLastButton()
    Invajter_scroll_edit:SetText("")
    while InvajterSV["command"][i]~=nil do
        Invajter_scroll_edit:Insert(InvajterSV["command"][i].."\n")
        i=i+1
    end
end


function Invajter.button_add_OnClick()
    local txt = Invajter_add_edit_box:GetText()
    if txt then
        txt = string.lower(txt)
    end
    if LastButton == 0 then
        Invajter.HandleCommandList(InvajterSV["cmd"]["add"][1],txt)
        Invajter.button_fraze_OnClick()
    elseif LastButton == 1 then
        Invajter.HandleBlackList(InvajterSV["cmd"]["add"][1],txt)
        Invajter.button_blacklist_OnClick()
    elseif LastButton == 2 then
        Invajter.ChangeMaximum(Invajter_add_edit_box:GetText())
        Invajter.button_maximum_OnClick()
    elseif LastButton == 3 then
        --BIG LETTERS SHOULD BE KEPT HERE
        Invajter.HandleEcho(InvajterSV["cmd"]["set"][1],Invajter_add_edit_box:GetText())
        Invajter.button_echo_OnClick()
    end
    Invajter_add_edit_box:SetText("")
end


function Invajter.button_delete_OnClick()
    local txt = Invajter_add_edit_box:GetText()
    if txt then
        txt = string.lower(txt)
    end
    if LastButton == 0 then
        Invajter.HandleCommandList(InvajterSV["cmd"]["delete"][1],txt)
        Invajter.button_fraze_OnClick()
    elseif LastButton == 1 then
        Invajter.HandleBlackList(InvajterSV["cmd"]["delete"][1],txt)
        Invajter.button_blacklist_OnClick()
    elseif LastButton == 2 then
        Invajter.ToggleIncludeSelf(InvajterSV["include_self"])
    end
    Invajter_add_edit_box:SetText("")
end


function Invajter.button_maximum_OnClick()
    LastButton = 2
    Invajter.HighlightLastButton()
    Invajter_scroll_edit:SetText(InvajterSV["max"])
end


function Invajter.button_echo_OnClick()
    LastButton = 3
    Invajter.HighlightLastButton()
    Invajter_scroll_edit:SetText(InvajterSV["strings"]["echo"])
end


function Invajter.button_echo_toggle_OnClick()
    Invajter.HighlightLastButton()
    Invajter.ToggleEcho(InvajterSV["echo"])
end

function Invajter.button_close_OnClick()
    Invajter_frame:Hide()
    Frame = nil
end


function Invajter.HighlightLastButton()
    Invajter_button_fraze:UnlockHighlight()
    Invajter_button_blacklist:UnlockHighlight()
    Invajter_button_maximum:UnlockHighlight()
    Invajter_button_echo:UnlockHighlight()
    Invajter_button_delete:Show()
    Invajter_button_delete:SetText("Delete")
    Invajter_button_add:SetText("Add")
    if LastButton == 0 then -- commands
        Invajter_button_fraze:LockHighlight()
    elseif LastButton == 1 then -- blacklist
        Invajter_button_blacklist:LockHighlight()
    elseif LastButton == 2 then -- max
        if InvajterSV["include_self"] == 1 then
            Invajter_button_delete:SetText("Include Self: On")
        else
            Invajter_button_delete:SetText("Include Self: Off")
        end
        Invajter_button_add:SetText("Change Maximum")
        Invajter_button_maximum:LockHighlight()
    elseif LastButton == 3 then -- echo
        Invajter_button_delete:Hide()
        Invajter_button_add:SetText("Change Echo")
        Invajter_button_echo:LockHighlight()
    end
end


function Invajter.InitializeUI()
    Invajter_frame:Hide()
    Invajter.button_minimap_Reposition()
    if InvajterSV["on"] == 1 then
        Invajter_button_stav:SetText("Invajter On")
    else
        Invajter_button_stav:SetText("Invajter Off")
    end
    if InvajterSV["cache"] == 1 then
        Invajter_button_cache:SetText("Cache On")
    else
        Invajter_button_cache:SetText("Cache Off")
    end
    if InvajterSV["echo"] == 1 then
        Invajter_button_echo_toggle:SetText("Echo On")
    else
        Invajter_button_echo_toggle:SetText("Echo Off")
    end
    Invajter_scroll_edit:ClearFocus()
    Invajter_scroll_edit:EnableMouse(false)
    _,build = GetBuildInfo()
    if (tonumber(build) >= 12340) then
        Invajter_scroll_edit:EnableJoystick(false)
    end
    Invajter_scroll_edit:EnableMouseWheel(false)
    Invajter_scroll_edit:EnableKeyboard(false)
end