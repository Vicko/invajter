Invajter = {}
Invajter.version = 25.6
Invajter.language = UnitFactionGroup("player")
local Frame = nil
function Invajter.OnEvent(wtfisthis, event, arg1, arg2)
    if event == "ADDON_LOADED" and arg1 == "Invajter" then
        SLASH_Invajter1, SLASH_Invajter2 = "/Invajter", "/Inva"
        SlashCmdList["Invajter"] = Invajter.ChatCommandHandler
        Invajter.SavedVars()
    -- other AddOns might load before and throw errors if this is not initialized yet
    elseif InvajterSV and InvajterSV["on"] == 1 and (Invajter.IsLeader() or Invajter.GetMembers() == 0) then
        if event == "CHAT_MSG_WHISPER" then
            Invajter.InvMe(arg1, arg2)
        elseif event == "CHAT_MSG_SYSTEM" then
            Invajter.CacheWork(arg1)
        end
    else
        Invajter.ClearCache(false)
        Invajter.CacheSize = 0
    end
end


function Invajter.InvMe(prefix, plr_name)
    prefix=string.lower(prefix)
    plr_name=string.lower(plr_name)
    
    if Invajter.isOnBlackList(plr_name) then
        return
        
    elseif Invajter.invalidCommand(prefix) then
        return
        
    elseif Invajter.GetMembers()<Invajter.GetMax(true)
    and (Invajter.CacheSize == 0 or Invajter.GetMembers() > 0) then
        if (not Invajter.GetMember(plr_name)) then
           InviteUnit(plr_name)
        end
    elseif Invajter.GetMembers()<Invajter.GetMax(false) then
        if (not Invajter.GetMember(plr_name)) then
            Invajter.SaveToCache(plr_name)
        end
    else
        Invajter.WhisperEcho(plr_name)
    end
end


function Invajter.GetMembers()
    if GetNumRaidMembers() > 0 then
        return GetNumRaidMembers() - 1 -- do not include self
    else
        return GetNumPartyMembers()
    end
end


function Invajter.GetMax(cacheIncluded)
    if cacheIncluded == true then
        return InvajterSV["max"] - Invajter.CacheSize - InvajterSV["include_self"]
    end
    return InvajterSV["max"] - InvajterSV["include_self"]
end


function Invajter.IsLeader()
    if UnitIsPartyLeader("player") == 1 or IsRaidOfficer() == 1 then
        return true
    end
    return nil
end


function Invajter.GetMember(plr_name)
    if (UnitInParty(plr_name) or UnitInRaid(plr_name)) then
        return true
    end
    return nil
end


function Invajter.Reload()
    DisableAddOn("Invajter")
    ReloadUI()
    EnableAddOn("Invajter")
    ReloadUI()
end


function Invajter.WhisperEcho(plr_name)
    if (InvajterSV["echo"] == 0) then
        return
    end
    
    if not Invajter.GetMember(plr_name) and InvajterSV["strings"]["echo"] then
        SendChatMessage(InvajterSV["strings"]["echo"],"WHISPER",Invajter.language,plr_name)
    end
end


function Invajter.Print(msg)
    if not DEFAULT_CHAT_FRAME then
        return
    end
    DEFAULT_CHAT_FRAME:AddMessage("|cff601020"..msg)
end


local f = CreateFrame("Frame")
f:RegisterEvent("ADDON_LOADED")
f:RegisterEvent("CHAT_MSG_WHISPER")
f:RegisterEvent("CHAT_MSG_SYSTEM")
f:SetScript("OnEvent", Invajter.OnEvent)