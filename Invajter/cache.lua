Invajter.CacheSize = 0
local cacheIndex = 0
local InvajterCache = {}

function Invajter.HandleCache(cmd)
    if Invajter.cmd_toggle(cmd) then
        Invajter.ToggleCache(InvajterSV["cache"])
    elseif Invajter.cmd_on(cmd) then
        Invajter.ToggleCache(false)
    elseif Invajter.cmd_off(cmd) then
        Invajter.ToggleCache(true)
    else
        Invajter.Print("|cff20ff20Invajter Cache\n|cffff2020_____________|cff601020\n/Invajter "..InvajterSV["cmd"]["cache"][1].." "..InvajterSV["cmd"]["on"][1].." 'jmeno hrace'- Povoli invovat hrace z cache pameti\n/Invajter "..InvajterSV["cmd"]["cache"][1].." "..InvajterSV["cmd"]["off"][1].." - Zakaze invovat hrace z cache pameti")
    end
end


function Invajter.ToggleCache(bool)
    if bool == 1 then
        InvajterSV["cache"] = 0
        Invajter.Print("Invovani z cache je |cffff2020Vypnute")
        Invajter_button_cache:SetText("Cache Off")
    else
        InvajterSV["cache"] = 1
        Invajter.Print("Invovani z cache je |cffff2020Zapnute")
        Invajter_button_cache:SetText("Cache On")
    end
end


function Invajter.CacheWork(msg)
    plr_name,message=Invajter.GetCommand(msg)     -- reload case - reloading sends "Self declines your group invitation." -> bugged count
    if message=="declines your group invitation." and plr_name ~= UnitName("player") then
        Invajter.CacheSize=Invajter.CacheSize-1
        Invajter.InviteFromCache()
    elseif message=="joins the party." or message=="has joined the raid group." then
        Invajter.CacheSize=Invajter.CacheSize-1
        if Invajter.GetMembers() == Invajter.GetMax(false) then
            Invajter.ClearCache(true)
        elseif Invajter.GetMembers() == 1 then
            if InvajterSV["max"]+InvajterSV["include_self"] > 5 then
                ConvertToRaid()
            end
            if InvajterSV["cache"] == 1 then
                Invajter.InviteAllCached()
            end
        end
    elseif strsub(msg, 0,16)=="You have invited" then
        Invajter.CacheSize=Invajter.CacheSize+1
        Invajter.RemoveFromCache(plr_name)
    elseif message=="leave the group." or (Invajter.GetMembers() == 0 and Invajter.CacheSize == 0 and message=="leaves the party.") then
        Invajter.ClearCache(false)
    end
end


function Invajter.InviteAllCached()
    if InvajterSV["cache"] == 0 or cacheIndex == 0 then
        return
    end
    local tmp = 0
    while(InvajterCache[tmp] ~= nil and Invajter.GetMembers() < Invajter.GetMax(true)) do
        Invajter.InviteFromCache()
    end
end


function Invajter.InviteFromCache()
    if InvajterSV["cache"] == 0 or cacheIndex == 0 then
        return
    end
    
    InviteUnit(InvajterCache[0])
    local tmp = 0
    InvajterCache[tmp] = nil
    while(InvajterCache[tmp+1] ~= nil) do
        InvajterCache[tmp]=InvajterCache[tmp+1]
        InvajterCache[tmp+1]=nil
    end
    cacheIndex = cacheIndex - 1
end


function Invajter.RemoveFromCache(plr_name)
    if InvajterSV["cache"] == 0 or cacheIndex == 0 then
        return
    end    
    local i=0
    while InvajterCache[i]~=nil and InvajterCache[i]~=plr_name do
        i=i+1
    end
    if (InvajterCache[i]) then
        InvajterCache[i]=nil
        cacheIndex = cacheIndex - 1
    end
    repeat
        i=i+1
        InvajterCache[i-1]=InvajterCache[i]
    until InvajterCache[i]==nil
end


function Invajter.ClearCache(whisper)
    if InvajterSV["cache"] == 0 or cacheIndex == 0 then
        return
    end
    repeat
        if (whisper == true and InvajterSV["echo"] == 1 and InvajterCache[cacheIndex] and InvajterSV["strings"]["echo"]) then
            Invajter.WhisperEcho(InvajterCache[cacheIndex])
        end
        InvajterCache[cacheIndex] = nil
        cacheIndex = cacheIndex - 1
    until InvajterCache[cacheIndex] == nil
end


function Invajter.SaveToCache(plr_name)
    if InvajterSV["cache"] == 0 then
        return
    end
    i = 0
    while (InvajterCache[i]) do
        if InvajterCache[i] == plr_name then
            return
        end
        i = i+1
    end
    InvajterCache[cacheIndex] = plr_name
    cacheIndex = cacheIndex + 1
end