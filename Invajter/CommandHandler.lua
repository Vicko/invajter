function Invajter.ChatCommandHandler(msg)
    local handled = nil
    local cmd, subcmd = Invajter.GetCommand(msg)
    local cmd2, subcmd2 = Invajter.GetCommand(subcmd)
    if cmd~=nil then cmd=string.lower(cmd) end
    if cmd2~=nil then cmd2=string.lower(cmd2) end
    if Invajter.cmd_echo(cmd) then
        handled = true
        Invajter.HandleEcho(cmd2,subcmd2)
    end
    if subcmd~=nil then subcmd=string.lower(subcmd) end
    if subcmd2~=nil then subcmd2=string.lower(subcmd2) end
    
    if not handled then
        if Invajter.cmd_toggle(cmd) then
            Invajter.Toggle(InvajterSV["on"])
        elseif Invajter.cmd_on(cmd) then
            Invajter.Toggle(0)
        elseif Invajter.cmd_off(cmd) then
            Invajter.Toggle(1)
        elseif Invajter.cmd_maximum(cmd) then
            Invajter.ChangeMaximum(subcmd)
        elseif Invajter.cmd_command(cmd) then
            Invajter.HandleCommandList(cmd2, subcmd2)
        elseif Invajter.cmd_blacklist(cmd) then
            Invajter.HandleBlackList(cmd2, subcmd2)
        elseif Invajter.cmd_cache(cmd) then
            Invajter.HandleCache(subcmd)
        elseif Invajter.cmd_reload(cmd) then
            Invajter.Reload()
        elseif Invajter.cmd_alternative(cmd) then
            Invajter.HandleAlternatives(subcmd)
        else
            local status=""
            if InvajterSV["on"] == 1 then
                status="|cffff2020Zapnuty|cff601020"
            else
                status="|cffff2020Vypnuty|cff601020"
            end
            Invajter.Print("|cff20ff20Invajter ver. "..Invajter.version..": "..status.." (max velikost groupy: |cffff2020"..InvajterSV["max"].."|cff601020.)\n|cffff2020_________________________|cff601020\n/Invajter "..InvajterSV["cmd"]["toggle"][1].." - Zapina / vypina Invajter.\n/Invajter "..InvajterSV["cmd"]["maximum"][1].." ## - nastavuje maximalni pocet lidi, ktere muzete invnout\n/Invajter "..InvajterSV["cmd"]["command"][1].." "..InvajterSV["cmd"]["help"][1].." - zobrazi informace ohledne commandu\n/Invajter "..InvajterSV["cmd"]["blacklist"][1].." "..InvajterSV["cmd"]["help"][1].." - zobrazi informace k blacklistu\n/Invajter "..InvajterSV["cmd"]["echo"][1].." - umozni nastavit automatickou odpoved pri plne group\n/Invajter "..InvajterSV["cmd"]["cache"][1].." - umozni zmenit invite z cache\n/Invajter "..InvajterSV["cmd"]["alternative"][1].." umozni modifikovat prikazy (napr. 'add' zamenit za 'pridej'")
        end
    end
end


function Invajter.FindAlternatives(cmd)
    if Invajter.cmd_maximum(cmd) then
        return "maximum"
    elseif Invajter.cmd_help(cmd) then
        return "help"
    elseif Invajter.cmd_blacklist(cmd) then
        return "blacklist"
    elseif Invajter.cmd_command(cmd) then
        return "command"
    elseif Invajter.cmd_add(cmd) then
        return "add"
    elseif Invajter.cmd_delete(cmd) then
        return "delete"
    elseif Invajter.cmd_cache(cmd) then
        return "cache"
    elseif Invajter.cmd_on(cmd) then
        return "on"
    elseif Invajter.cmd_off(cmd) then
        return "off"
    elseif Invajter.cmd_toggle(cmd) then
        return "toggle"
    elseif Invajter.cmd_echo(cmd) then
        return "echo"
    elseif Invajter.cmd_set(cmd) then
        return "set"
    elseif Invajter.cmd_reload(cmd) then
        return "reload"
    elseif Invajter.cmd_alternative(cmd) then
        return "alternative"
    else
        return nil
    end
end


function Invajter.cmdHelper(cmd,string)
    if not cmd then
        return nil
    end
    local x=0
    repeat
        if (cmd==(InvajterSV["cmd"][string][x])) then
        return true
        end
        x=x+1
    until ((plr_name)==(InvajterSV["cmd"][string][x])) or (InvajterSV["cmd"][string][x]==nil)
    return nil
end


function Invajter.cmd_maximum(cmd)
    return Invajter.cmdHelper(cmd,"maximum")
end


function Invajter.cmd_help(cmd)
    return Invajter.cmdHelper(cmd,"help")
end


function Invajter.cmd_blacklist(cmd)
    return Invajter.cmdHelper(cmd,"blacklist")
end


function Invajter.cmd_command(cmd)
    return Invajter.cmdHelper(cmd,"command")
end


function Invajter.cmd_add(cmd)
    return Invajter.cmdHelper(cmd,"add")
end


function Invajter.cmd_delete(cmd)
    return Invajter.cmdHelper(cmd,"delete")
end


function Invajter.cmd_cache(cmd)
    return Invajter.cmdHelper(cmd,"cache")
end


function Invajter.cmd_on(cmd)
    return Invajter.cmdHelper(cmd,"on")
end


function Invajter.cmd_off(cmd)
    return Invajter.cmdHelper(cmd,"off")
end


function Invajter.cmd_toggle(cmd)
    return Invajter.cmdHelper(cmd,"toggle")
end


function Invajter.cmd_echo(cmd)
    return Invajter.cmdHelper(cmd,"echo")
end


function Invajter.cmd_set(cmd)
    return Invajter.cmdHelper(cmd,"set")
end


function Invajter.cmd_alternative(cmd)
    return Invajter.cmdHelper(cmd,"alternative")
end


function Invajter.cmd_show(cmd)
    return Invajter.cmdHelper(cmd,"show")
end


function Invajter.cmd_reload(cmd)
    return Invajter.cmdHelper(cmd,"reload")
end


function Invajter.GetCommand(msg)
     if msg then
         local a,b,c=strfind(msg, "(%S+)")
         if a then
             return c, strsub(msg, b+2)
         else
             return ""
         end
     end
 end


function Invajter.invalidCommand(cmd)
    local i=0
    while InvajterSV["command"][i] ~= nil do
        if cmd == InvajterSV["command"][i] then
            return nil
        end
        i=i+1
    end
    return true
end