function Invajter.isOnBlackList(plr_name)
    local k=0
    while InvajterSV["blacklist"][k] ~= nil do
        if (plr_name==(InvajterSV["blacklist"][k])) then
            return true
        end
        k=k+1
    end
    return nil
end


function Invajter.HandleBlackList(cmd, subcmd)
    if Invajter.cmd_add(cmd) then
        local J=0
        while InvajterSV["blacklist"][J]~=nil and InvajterSV["blacklist"][J]~=subcmd do
            J=J+1
        end
        InvajterSV["blacklist"][J]=subcmd
    elseif Invajter.cmd_delete(cmd) then
        local J=0
        while InvajterSV["blacklist"][J]~=nil and InvajterSV["blacklist"][J]~=subcmd do
            J=J+1
        end
        InvajterSV["blacklist"][J]=nil
        repeat
            J=J+1
            InvajterSV["blacklist"][J-1]=InvajterSV["blacklist"][J]
        until InvajterSV["blacklist"][J]==nil
    elseif Invajter.cmd_help(cmd) then
        Invajter.Print("|cff20ff20Invajter BlackList\n|cffff2020_____________|cff601020\n/Invajter "..InvajterSV["cmd"]["blacklist"][1].." "..InvajterSV["cmd"]["add"][1].." 'jmeno hrace'- prida hrace na BlackList\n/Invajter "..InvajterSV["cmd"]["blacklist"][1].." "..InvajterSV["cmd"]["delete"][1].." 'jmeno hrace' - odstrani hrace z BlackListu")
    else
        local J=0
        if  InvajterSV["blacklist"][0]==nil then
            Invajter.Print("Seznam blokovanych jmen je prazdny")
        else
            Invajter.Print("Seznam blokovanych hracu:")
            while InvajterSV["blacklist"][J]~=nil do
                Invajter.Print("'"..InvajterSV["blacklist"][J].."'")
                J=J+1
            end
        end
    end
end


function Invajter.HandleCommandList(cmd, subcmd)
    if Invajter.cmd_add(cmd) then
        local J=0
        while InvajterSV["command"][J]~=nil and InvajterSV["command"][J]~=subcmd do
            J=J+1
        end
        InvajterSV["command"][J]=subcmd
    elseif Invajter.cmd_delete(cmd) then
        local J=0
        while InvajterSV["command"][J]~=nil and InvajterSV["command"][J]~=subcmd do
            J=J+1
        end
        InvajterSV["command"][J]=nil
        repeat
            J=J+1
            InvajterSV["command"][J-1]=InvajterSV["command"][J]
        until InvajterSV["command"][J]==nil
    elseif Invajter.cmd_help(cmd) then
        Invajter.Print("|cff20ff20Invajter Command\n|cffff2020_____________|cff601020\n/Invajter "..InvajterSV["cmd"]["command"][1].." "..InvajterSV["cmd"]["add"][1].." - prida prikaz pro invite\n/Invajter "..InvajterSV["cmd"]["command"][1].." "..InvajterSV["cmd"]["delete"][1].."  -  odstrani prikaz pro invite")
    else
        local J=0
        if  InvajterSV["command"][0]==nil then
            Invajter.Print("Seznam prikazu pouzitelnych pro invite")
        else
            Invajter.Print("Seznam prikazu pouzitelnych pro invite:")
            while InvajterSV["command"][J]~=nil do
                Invajter.Print("'"..InvajterSV["command"][J].."'")
                J=J+1
            end
        end
    end
end

