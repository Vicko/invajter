﻿# ![logo](http://4.bp.blogspot.com/-Nwob3rmygpE/UaeziQ9I6yI/AAAAAAAAEnI/JkfV_Y0FYro/s1600/rainbow_dash_by_lookitslaurie-d4qyjuk.png)




_____________
## Basic control



##Menu

/invajter - zobrazi dostupne prikazy



##Zapinani / Vypinani

/invajter toggle - vypne/zapne invajter

/invajter on - zapne invajter

/invajter off - vypne invajter



##Invite prikazy

Prikazy umoznujici nastavit commandy, na ktere bude AddOn invitovat ostatni lidi.

/invajter command - zobrazi veskere commandy, na ktere AddOn reaguje

/invajter command help - zobrazi napovedu ohledne commandu

/invajter command add - prida command, na ktery AddOn bude reagovat (napr. inv)

/invajter command delete - odstrani command, na ktery bude AddOn reagovat



##Velikost group

Nastavi maximalni pocet lidi, ktere AddOn invne do groupy (nepocita majitele AddOnu), invne vzdy maximalne dany pocet, nikdy vic

/invajter maximum



##Blacklist pro zlobivce

Umozni AddOnu nereagovat na whispy lidi na cerne listine (napr. lidi co umyslne neco sabotuji)

/invajter blacklist - zobrazi seznam lidi, kteri jsou v blacklistu

/invajter blacklist help - zobrazi napovedu ohledne blacklistu

/invajter blacklist add - prida hrace na blacklist

/invajter blacklist delete - odstrani hrace z blacklistu



##Cache

Pri zapnuti cache si AddOn zapamatuje nick hrace, ktery napsal v dobe, kdy nebyl maximalni pocet lidi, ale zaroven 
byl rozeslany limitovany pocet invite, nebo byl odeslan 1. invite a nebyl doposud prijat (nelze poslat vice jak 2 invite naraz, pokud neni jiz zalozena group. Pote co nektery hrac odmitne invite, AddOn automaticky invituje dalsiho hrace z cache.

/invajter cache - zobrazi informace ohledne cache

/invajter cache on - zapne invite lidi z cache

/invajter cache off - vypne invite lidi z cache


##Echo

Umozni odesilat automatickou odpoved na inv, pokud je maximalni pocet lidi v groupe (raidu)

/invajter echo - zobrazi napovedu ohledne echa

/invajter echo on - zapne automatickou reakci na plny pocet lidi

/invajter echo off - vypne automatickou reakci na plny pocet lidi

/invajter echo set "text" - nastavi "text" jako automatickou reakci na plny pocet lidi ("text" bez uvozovek)


##alternative

Umozni modifikovat prikazy

/invajter alternative add 'a' 'b' - prida k aktualnimu prikazu 'a' alternativni prikaz 'b'

napriklad: /invajter alternative command prikaz umozni pouzivat /invajter prikaz namisto /invajter command

/invajter alternative delete 'a' 'b' - smaze prikaz 'b', ktery ma stejny vyznam jako prikaz 'a'

napriklad pokud pouzijeme predchozi prikaz na pridani /invajter alternative command prikaz, tak /invajter alternative delete prikaz command zakaze pouzivani /invajter command a zanecha pouze /invajter prikaz
_____________

